import numpy as np


class Vec:
    """
    A class representing a vector in 3 dimensions.

    Attributes:
        x : float or int
        y : float or int

    Methods:
        __init__(self, x, y, z)
        Operator |
        Operator +
        Operator *
        abs(self)
    """

    def __init__(self, x, y):
        """
        Initialize a new instance of vector
        """
        self.x = x
        self.y = y

    def __str__(self):
        """
        return a string for the class vector as "Vector(x,y,z)"
        """
        return f"Vector({self.x}, {self.y})"

    def __add__(self,other):
        
        """
        Overload the + Operator for the class Vector
        Implements the summation of two instances of class Vector
        """
        
        x = self.x + other.x
        y = self.y + other.y
        
        return(Vec(x,y))
        

    def __mul__(self,value):
        
        """
        Mutliplies vector by value or other vector
        """
        
        if isinstance(value,Vec):
            return((self.x*value.x+self.y*value.y+self.z*value.z))
            
        else:
            x = self.x * value
            y = self.y * value
            return(Vec(x,y))

    
    def abs(self):
        """
        Return the absolute value of the Vector instance.
        """
        return((self.x**2+self.y**2+self.z**2)**(1/2))
    
    
    def __or__(self,other):
        '''
        gives the dstance between two points
        
        __or__ refers to |  
        '''
        return ((self.x-other.x)**(1/2)+(self.y-other.y)**2)**(1/2)
    
    
    def rotate(self,angle):
        '''
        rotates self
        '''
        self.x = self.x * np.cos(angle) - self.y * np.sin(angle)
        self.y = self.x * np.sin(angle) + self.y * np.cos(angle)




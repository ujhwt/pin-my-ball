import pygame
from pathlib import Path
import numpy as np
import copy

# Initialize PyGame
pygame.init()
pygame.font.init()
font = pygame.font.Font(None, 40)
text_color = (0,0,0)

# Initial window size
s_width = 600
s_height = 800

# Define spacetime 
GRAVITY_X = 0.0
GRAVITY_Y = 0.03
DT = 1 # ms (discretization of time) 

collidecountdown = 0
mousestate = False

# Making display screen
screen = pygame.display.set_mode((s_width, s_height), pygame.RESIZABLE)
bg_orig=pygame.image.load(Path(__file__).parents[0] / Path("bkg.jpg")).convert()


clock = pygame.time.Clock()

# Setup 
running = True



# important funcitons

def rotation_matrix(point,angle):
    return([point[0]*np.cos(angle)-point[1]*np.sin(angle),point[0]*np.sin(angle)+point[1]*np.cos(angle)])

def distance(vec1,vec2=[0,0]):
    '''
    returns the distance between two vectors
    
    If given only one argument, will return lenth
    
    '''
    return(((vec1[0]-vec2[0])**2+(vec1[1]-vec2[1])**2)**(1/2))

def add(vec1,vec2):
    return([vec1[0]+vec2[0],vec1[1]+vec2[1]])

def sub(vec1,vec2):
    return([vec1[0]-vec2[0],vec1[1]-vec2[1]])
    


    


class Ball:
    

    
    def __init__(self,x,y,vx,vy,Radius):
        self.position = [x,y]
        self.speed = [vx,vy]
        self.rad = Radius
        

class Polygon:
    
    def __init__(self,points,color,middle=0,rotation_speed=0,rotationdirection=0,dampening=1,push={'speed':0,'direction':1,'distance':0,'angle':0}):
        self.color = pygame.Color(color)
        self.collidecountdown = 0
        self.dampening = dampening
        self.rotation_speed = rotation_speed
        self.rotationdirection = rotationdirection
        self.direction = 0
        if middle == 0:
          a = 0
          b = 0
          for element in points:
            a += element[0]
            b += element[1]
          self.middle = [a/len(points),b/len(points)]
        else:
            self.middle = middle
        self.start = copy.copy(self.middle)
        self.middle_speed = [0,0]
          
        self.corners = []  
        for element in points:
            self.corners.append([element[0]-self.middle[0],element[1]-self.middle[1]])
        self.curve_orientation = self.find_curve_orientation()
        self.furthest_point = self.find_furthest_point()
        self.furthest_point_start = copy.copy(self.find_furthest_point())
        self.devolplist = {'move':False,'middle':False,'rotation':False,'distortion':False,'corners':[False for _ in range(len(self.corners))]}
        self.push = push
        
        
    def draw(self):
        if self.collidecountdown > 0:
            color = [0,self.color[1],self.color[2]]
        else:
            color = self.color
        
        points = []
        for element in self.corners:
            points.append([element[0]+self.middle[0],element[1]+self.middle[1]])
        pygame.draw.polygon(screen, color ,points)
        
        if devmode == True:
            
            pygame.draw.circle(screen,(255, 255, 255, 255),self.middle,self.furthest_point,1)
            pygame.draw.circle(screen,(255, 255, 255, 255),self.middle,5)
            
            point_on_circle = add(self.middle,[0,-self.furthest_point])
            pygame.draw.line(screen,(100, 0, 0),self.middle,point_on_circle,1)
            pygame.draw.circle(screen,(100, 0, 0),point_on_circle,5)
            
            aloha = add(self.middle,[self.furthest_point,self.rotation_speed*1000])
            
            pygame.draw.line(screen,(0, 100, 0),add(self.middle,[self.furthest_point,0]),aloha,2)
            pygame.draw.circle(screen,(0,100,0),aloha,5)
            
            #pygame.draw.circle(screen,(0, 0, 255),add(self.middle,[-self.furthest_point,0]),5)
            #pygame.draw.line(screen,(100, 0, 0),add(self.start,[-self.furthest_point,0]),add(self.middle,[-self.furthest_point,0]),1)
            
            
            if click == True:
                
                if distance(mouse,aloha) < 10:
                    if self.devolplist['rotation'] == True:
                        self.devolplist['rotation'] = False
                    else:
                        self.devolplist['rotation'] = True

                if distance(mouse,self.middle) < 10:
                    if self.devolplist['middle'] == True:
                        self.devolplist['middle'] = False
                    else:
                        self.devolplist['middle'] = True
                        
                if distance(mouse,point_on_circle) < 10:
                    if self.devolplist['distortion'] == True:
                        self.devolplist['distortion'] = False
                    else:
                        self.devolplist['distortion'] = True
                
                #if distance(mouse,add(self.middle,[-self.furthest_point,0])) < 10:
                #    if self.devolplist['move'] == True:
                #        self.devolplist['move'] = False
                #    else:
                #        self.devolplist['move'] = True
                #        self.start = copy.copy(self.middle)
                        
                
                 
                for element in range(0,len(self.corners)):       
                    if distance(mouse,add(self.middle,self.corners[element])) < 10:
                        if self.devolplist['corners'][element] == True:
                            self.devolplist['corners'][element] = False
                            
                        else:
                            self.devolplist['corners'][element] = True    
                


                

                
            if self.devolplist['rotation'] == True:
                self.rotation_speed -= (aloha[1]-mouse[1])/1000
        
                
            if self.devolplist['distortion']:
                distortion = abs(distance(mouse,self.middle)/self.furthest_point)

                
                for element in range(0,len(self.corners)):
                    self.corners[element][0] *= distortion
                    self.corners[element][1] *= distortion
                self.furthest_point = self.find_furthest_point()
                    
            if self.devolplist['middle'] == True:
                self.middle = mouse
                
            #if self.devolplist['move'] == True:
            #    self.push['distance'] = distance(add(self.start,[-self.furthest_point,0]),mouse) 
            #    self.push['angle'] = np.arctan((add(self.start,[-self.furthest_point,0])[1]-mouse[1])/(add(self.start,[-self.furthest_point,0])[0]-mouse[0]))
            #   self.push['speed'] = 2
             #   pygame.draw.line(screen,(0,0,250),add(self.start,[-self.furthest_point,0]),mouse,1)

                                    
         
               
                
            for element in  range(0,len(self.corners)):
                pygame.draw.line(screen,(255, 255, 255, 255),self.middle,(add(self.middle,self.corners[element])),1)
                pygame.draw.circle(screen,(255, 255, 255, 255),(add(self.middle,self.corners[element])),3)
                if self.devolplist['corners'][element] == True:  
                    self.corners[element] = sub(mouse,self.middle)
                    self.furthest_point = self.find_furthest_point()
    
                

        
        
        
        
    def find_furthest_point(self):
        '''
        finds the furthes distance any point has to the center of the polygon
        '''
        return(max(list(map(distance, self.corners)))) 
    
    #def find_closest_point(self):
    #    return(min(list(map(distance, self.corners))))
       
       
        
    def find_curve_orientation(self):
        '''
        finds the orientation in which the polygon has been drawn
        '''
        
        lines = self.lines()
        
        xlist = [element[0][0] for element in lines]
        
        biggest_x = xlist.index(max(xlist))
        
        B = lines[biggest_x-1][1]
        A = lines[biggest_x][1]
        
        if A > np.pi + B:
            return(0)
        else:
            return(1)

            
    def lines(self):
        '''
        creates a list of all the lines, making up the polygon
        
        returns as list [[point],angle,lenth] of the line
        '''
    
        line = []
        
    
    
        for element in range(0,len(self.corners)):
           P1 = self.corners[element-1]
           P2 = self.corners[element]
           
        
           lenght0 = distance(P1,P2)

           if P1[0]-P2[0] == 0:
               if P1[1] < P2[1]:
                   angle = np.pi/2
               else:
                   angle = -np.pi/2
               
               angle = -np.pi/2
           elif P1[0]-P2[0] < 0:
               angle = np.arctan((P2[1]-P1[1])/((P2[0]-P1[0])))
           else:
               angle = np.pi + np.arctan((P1[1]-P2[1])/((P1[0]-P2[0])))
            
           line.append([P1,angle,lenght0])
        
    
        
        return(line)
    
    
    def collide(self):
        self.collidecountdown -= 1
        
        didcollide = False
        
        if distance(ball.position,self.middle) > self.furthest_point+2*ball.rad:
            pass
        
        elif self.collidecountdown <= 0:
            for element in self.lines():

                rel = rotation_matrix(element[0],-element[1])
                
                pos = rotation_matrix([ball.position[0]-self.middle[0],ball.position[1]-self.middle[1]],-element[1])
                
                angle = element[1]
                     
                if 0 < pos[0] - rel[0]  < element[2] and ball.rad > pos[1] - rel[1] + self.curve_orientation*ball.rad > 0:

                    k = pos[0]*self.rotation_speed*self.rotationdirection + rotation_matrix(self.middle_speed,-element[1])[1]
                    
                    ball.speed = rotation_matrix(ball.speed,-element[1])
                    
                    ball.speed = [ball.speed[0],-self.dampening*ball.speed[1]+2*k]
                    
                    ball.speed = rotation_matrix(ball.speed,element[1])
                    
                    self.collidecountdown = 10
                    
                    didcollide = True
                
                         
        elif didcollide == False and self.collidecountdown <= 0:
            
            for element in self.lines():
                
                xrel = ball.position[0]-element[0][0] - self.middle[0]
                yrel = ball.position[1]-element[0][1] - self.middle[1]
                
                
                if distance([xrel,yrel]) < ball.rad:
                    
                    if xrel > 0:
                        angle = np.arctan(yrel/xrel)
                    else:
                        angle = -np.pi + np.arctan(yrel/xrel)
                        
                    corner_speed = []
                    
                    corner_speed.append(self.middle_speed[0] + self.rotation_speed*self.rotationdirection*element[0][1])
                    
                    corner_speed.append( self.middle_speed[1] + self.rotation_speed*self.rotationdirection*element[0][0])
                    
                    k = rotation_matrix(corner_speed,angle+np.pi/2)[1]

                        
                    ball.speed = rotation_matrix(ball.speed,-angle-np.pi/2)
                        
                    ball.speed = [ball.speed[0],-self.dampening*ball.speed[1]+2*k]

                    ball.speed = rotation_matrix(ball.speed,+angle+np.pi/2)
              
                    
                    
                
            
    
    
        
    
    
    
    def rotate(self,angle):
        
        """
        phi: Rotation per Frame

        Das Objekt wird um seinen Mittelpunkt rotiert
        """
        
        self.corners = [rotation_matrix(element,angle) for element in self.corners]
    
    
        
    def move(self):
        
        """
        Das Objekt wird hin und her bewegt
         
        phi : Winkel der Auslenkung 
     
        speed : Geschwindigkeit
         
        length : Distanz der Verschiebung in beide Richtungen
        """
        if self.push['speed'] > 0:
            
            dx = (self.middle[0]-self.start[0])**2
            
            dy = (self.middle[1]-self.start[1])**2
            
            k = self.push['speed']*2 * abs((self.push['distance']**2 - dx -dy)/self.push['distance']**2)
            
            self.push['speed'] += k


        
            if dx + dy > self.push['distance']:
                if self.push['direction'] == 0:
                    self.push['direction'] = 1
                else:
                    self.push['direction'] = 0
            
            if self.direction != 0:
                self.push['speed'] = -self.push['speed']
                
            self.middle[0] += np.cos(self.push['angle'])*self.push['speed']
            self.middle[1] += np.sin(self.push['angle'])*self.push['speed']
            
            self.middle_speed = [np.cos(self.push['angle'])*self.push['speed'], np.sin(self.push['angle'])*self.push['speed']]
            
            print('move!')
            
        
        
        
            
            
        

            

ball = Ball(490,100,0,3,10)

object_1 = Polygon([[300,300],[275,350],[300,400],[500,400],[500,300]],[100,100,100],0,0.01,1,0.8)

object_2 = Polygon([[100,100],[125,150],[100,200],[150,175],[200,200],[200,100]],[200,0,100],[300,400],0,1,0.8)

start_objects = [object_1,object_2]

object_list = start_objects

listo = [[(44, 66), (114, 117), (156, 30)], [(180, 51), (151, 119), (365, 42)], [(249, 108), (304, 172), (326, 114)], [(344, 82), (441, 157), (488, 85)], [(412, 34), (537, 72), (537, 16)], [(509, 91), (513, 212), (577, 108), (593, 95)], [(433, 202), (361, 153), (411, 259), (581, 252)], [(328, 177), (378, 289), (266, 284)], [(229, 125), (94, 150), (273, 194)], [(36, 164), (153, 225), (47, 249)], [(172, 199), (260, 227), (229, 265)], [(178, 245), (210, 329), (73, 281)], [(241, 296), (416, 384), (310, 384)], [(404, 289), (452, 374), (549, 360), (484, 272)], [(534, 283), (583, 350), (585, 279)], [(571, 383), (475, 400), (584, 477)], [(559, 495), (430, 405), (491, 534), (556, 669)], [(400, 421), (312, 416), (289, 518), (455, 544)], [(482, 614), (481, 566), (443, 612)], [(232, 342), (277, 428), (130, 380)], [(274, 463), (239, 547), (14, 355)], [(276, 552), (343, 576), (290, 649)], [(389, 591), (426, 681), (336, 660)], [(479, 650), (567, 703), (462, 741)], [(50, 458), (99, 470), (55, 517)], [(112, 519), (51, 550), (102, 583)], [(129, 505), (150, 562), (162, 517)], [(194, 555), (169, 608), (231, 608)], [(136, 581), (147, 639), (94, 639)], [(34, 581), (79, 590), (51, 634)], [(170, 628), (219, 666), (166, 687)], [(230, 633), (267, 633), (260, 715)], [(295, 677), (337, 680), (319, 734)], [(369, 705), (425, 709), (360, 747)], [(177, 707), (255, 717), (185, 747)], [(86, 672), (148, 710), (109, 731)], [(27, 678), (77, 730), (28, 748)]]

listofcorners = []



#for element in listo:
#    object_list.append(Polygon(element,(100,100,100),dampening=0.9))


if object_list is None:
    object_list = []
    for element in start_objects:
       object_list.append(element)

def add_object(element):
    global object_list
    object_list.append(element)
    return object_list



print(object_list)


listofcorners = []

newobject_2 = 0

devmode = False
draw_mode = False
mousestate = False

red = 0
green = 0
blue = 0

fixated = False

click = False
right_click = False

####
# Main event loop
while running:
    
    click = False
    right_click = False
    
    
    
    #if devmode == True:
    #font = pygame.font.Font(None, 40)
    #text_color = (0,0,0)
    #text = font.render("Developer mode",True,(0,0,0))
    #screen.blit(text,(10,10))
    
        
    
    

    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            # Tastaturtaste wurde gedrückt

            if event.key == pygame.K_SPACE:
                object_2.rotation_speed = -0.01
            if event.key == pygame.K_ESCAPE:
                running = False
            if event.key == pygame.K_a:
                fixated = False
                devmode = True
                print("Developer mode is activated")
                newobject_2 = 0
                listofcorners = []
            if event.key == pygame.K_l:
                devmode = False
            if devmode == True and event.key == pygame.K_0:
                draw_mode = True
            
            
            
            if draw_mode == True:
                if event.key == pygame.K_p:
                    devmode = False
                    print("Developer mode is deactivated")
                if event.key == pygame.K_s:
                    object_list.append(copy.copy(newobject_2))
                    print("new object has been saved")
                    listofcorners = []
                    draw_mode = False
                if event.key == pygame.K_r:
                    if red < 240:
                       red += 20
                    else:
                        red = 0
                if event.key == pygame.K_g:
                    if green < 240:
                       green += 20
                    else:
                        green = 0
                if event.key == pygame.K_b:
                    if blue < 240:
                       blue += 20
                    else:
                        blue = 0
                if event.key == pygame.K_d:
                    listofcorners =listofcorners[:-1]
                    if len(listofcorners) > 2:
                        newobject_2 = Polygon(listofcorners,[red,green,blue])
                    else:
                        newobject_2 = 0
                        listofcorners = []
    
                    
                    
            
                
        if devmode == True:
            mouse = pygame.mouse.get_pos()
                
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                print("Linke Maustaste wurde geklickt!")
                click = True
                if mousestate == True:
                    mousestate = False

                else:
                    mousestate = True
                if draw_mode == True:
                    listofcorners.append(mouse)
                    if len(listofcorners) > 2:
                        newobject_2 = Polygon(listofcorners,[red,green,blue])
                        
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                right_click=True

                
                
           # if event.key == 
                
        elif event.type == pygame.KEYUP:
            # Tastaturtaste wurde losgelassen
            object_2.rotation_speed = 0.01
        continue
    
    collidecountdown += 1
    
    
    
    
  


   
    # Adjust screen
    s_width, s_height = screen.get_width(), screen.get_height()
    bg = pygame.transform.scale(bg_orig, (s_width, s_height))
    screen.blit(bg, (0, 0)) # redraws background image


    

    if draw_mode == True:
        if newobject_2 != 0:
           newobject_2 = Polygon(listofcorners,[red,green,blue])
           newobject_2.draw()
           newobject_2.collide()
        
    
    if draw_mode == True:
        if collidecountdown*2+1 < 256:
            a = (collidecountdown*2,collidecountdown*2,collidecountdown*2)
        else:
            collidecountdown = 20
            a = (collidecountdown*2,collidecountdown*2,collidecountdown*2)
        
        coloroflines = collidecountdown
        if len(listofcorners) > 0:
           pygame.draw.line(screen, a,listofcorners[-1], mouse, width=5)
           pygame.draw.line(screen, a, listofcorners [0], mouse, width=5)
           
        if len(listofcorners) == 2:
           pygame.draw.line(screen, a,listofcorners[0], listofcorners[1], width=5)
            
           
        
    
    

  

    ball.speed[1] += GRAVITY_Y*DT

    if ball.position[1] >= screen.get_height():
        ball.speed[1] = ball.speed[1] * (-1)
    if ball.position[1] <= 0:
        ball.speed[1] = ball.speed[1] * (-1)
    if ball.position[0]>= 600 or ball.position[0] <=0 :
        ball.speed[0] = ball.speed[0] * (-1)

    ball.position[1] += ball.speed[1]*DT + 0.5 * GRAVITY_Y*DT**2
    ball.position[0] = ball.position[0] + ball.speed[0]*DT

    pygame.draw.circle(screen, (35, 161, 224), ball.position , ball.rad)


   
    #object_1.push(np.pi/4,0.8,200,3)
    for element in object_list:
        element.rotate(element.rotation_speed)
        element.draw()
        element.collide()
        element.move()


    
    #print(np.sqrt(ball.speed[0]**2+ball.speed[1]**2))
    
    #object_1.push(np.pi/2,1,200,0)
    
    pygame.display.flip() # Update the display of the full screen
    clock.tick(120) # 120 frames per second


# Done! Time to quit.
print("Hello")


